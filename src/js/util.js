/**
 * @file A collection of utility functions.
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

module.exports = class Util
{
    static kebabCase(str)
    {
        return str
            .replace(/([a-z])([A-Z])/g, '$1-$2')
            .replace(/\s+/g, '-')
            .toLowerCase()
    }

    // Polyfill for "getComposedPath". Adapted from code by @leofavre on GitHub.
    static getComposedPath(evt)
    {
        var path = (evt.composedPath && evt.composedPath()) || evt.path,
            target = evt.target

        if (path != null) {
            // Safari doesn't include Window, and it should.
            path = (path.indexOf(window) < 0) ? path.concat([window]) : path
            return path
        }

        if (target === window) {
            return [window]
        }

        function getParents(node, memo) {
            memo = memo || []
            var parentNode = node.parentNode

            if (!parentNode) {
                return memo
            }
            else {
                return getParents(parentNode, memo.concat([parentNode]))
            }
        }

        return [target]
            .concat(getParents(target))
            .concat([window])
    }

    // Scroll position polyfill:
    static get scroll()
    {
        const de = document.documentElement
        return {
            x: window.scrollX || window.pageXOffset || de.scrollLeft,
            y: window.scrollY || window.pageYOffset || de.scrollTop,
            scrollbarWidth: window.innerWidth - de.clientWidth
        }
    }
}
