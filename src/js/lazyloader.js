/**
 * @file Postpones image loading until the DOM is ready.
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

module.exports = class Lazyloader
{
    constructor()
    {
        const prefix = 'data-lazy-'
        window.addEventListener('load', () =>
        {
            Array.from(document.body.getElementsByTagName('*'))
                .forEach(el =>
                {
                    const lazyAttributes = Object.values(el.attributes)
                        .filter(key => key.name.startsWith(prefix))
                    if (lazyAttributes.length === 0)
                    {
                        return false
                    }

                    lazyAttributes.forEach(attribute =>
                    {
                        const name = attribute.name.slice(prefix.length)
                        el.setAttribute(name, attribute.value)
                        el.removeAttribute(attribute.name)
                    })
                })
        })
    }
}
