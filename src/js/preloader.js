/**
 * @file Removes the preload screen when the DOM is ready:
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

module.exports = class Preloader
{
    constructor()
    {
        this.hasLoaded = false
        this.$el = document.getElementById('preloader')

        // Add artificial delay to the start page for the jumbotron video:
        const path = window.location.pathname
        const delay = path === '/' || path === '/wordpress/' || path === '/en/'
            ? 2000
            : 0

        // If too long time has passed, load anyway:
        setTimeout(this.load.bind(this), 5000)

        // Load the page:
        window.addEventListener('load', () =>
        {
            setTimeout(this.load.bind(this), delay)
        })
    }

    load()
    {
        if (this.hasLoaded)
        {
            return true
        }

        // Initialize the DOM:
        this.hasLoaded = true
        document.body.classList.add('has-loaded')
        this.$el.style.opacity = 0
        this.$el.addEventListener('webkitTransitionEnd', () =>
        {
            this.$el.parentElement.removeChild(this.$el)
        })

        return true
    }
}
