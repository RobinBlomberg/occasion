/**
 * @file Initializes all Slick sliders.
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

module.exports = class Carousel
{
    constructor(el = '.carousel')
    {
        $(document).ready(() => $(el).slick({
            arrows:         true,
            autoplay:       true,
            autoplaySpeed:  2700,
            dots:           true,
            infinite:       true,
            speed:          1000,
            pauseOnFocus:   true,
            pauseOnHover:   true,
            lazyLoad:       'progressive',
        }))
    }
}
