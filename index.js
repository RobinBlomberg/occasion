/**
 * @file Compiles CSS and JS.
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

// Local WordPress folder:
const WORDPRESS_PATH = 'C:/XAMPP/htdocs/wordpress'

// Modules:
const fs      = require('fs')
const path    = require('path')
const sass    = require('node-sass')
const bundle  = require('./lib/bundle')

// Set output folders:
const destDir = path.join(WORDPRESS_PATH, 'wp-content/themes/occasion')
const file    = `${__dirname}/src/scss/index.scss`
const destCss = `${destDir}/style.css`
const srcJs   = `${__dirname}/src/js/index.js`
const destJs  = `${destDir}/js/app.js`

// Output CSS:
const css = sass.renderSync({ file }).css.toString()
fs.writeFileSync(destCss, css)

// Output JavaScript:
bundle(srcJs, destJs, 'IE >= 11, since 2014')
