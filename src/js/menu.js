/**
 * @file Handles the Menu logic.
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

const _ = require('./util')

module.exports = class Menu
{
    constructor()
    {
        this.expandY        = 96
        this.scrollbarWidth = _.scroll.scrollbarWidth
        this.openClass      = 'opened'
        this.$el            = $('.menu')
        this.$overlay       = $('.modal-overlay')
        this.$menuOpen      = this.$el.find('[data-action="open-menu"]')
        this.$contactOpen   = this.$el.find('[data-action="open-contact"]')
        this.$translateLink = this.$el.find('.mobile-translation-link')

        this.toggleExpanded()
        $(document).on('scroll', e => this.toggleExpanded())
        this.$menuOpen.on('click', e => this.toggle())
    }

    toggleExpanded()
    {
        this.$el.toggleClass('expanded', _.scroll.y >= this.expandY)
    }

    toggle()
    {
        if (!this.isOpen)
        {
            this.open()
        }
        else
        {
            this.close()
        }
    }

    get height()
    {
        return this.$el.height()
    }

    get isOpen()
    {
        return this.$el.hasClass(this.openClass)
    }

    open()
    {
        this.$overlay.addClass('dimmed')
        this.$el.addClass(this.openClass)
    }

    close()
    {
        this.$overlay.removeClass('dimmed')
        this.$el.removeClass(this.openClass)
    }
}
