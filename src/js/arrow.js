/**
 * @file Removes the scroll-down arrow on scroll.
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

module.exports = class Arrow
{
    constructor()
    {
        this.el = $('.long-arrow')
        $(document).on('scroll', e => this.el.addClass('disappeared'))
    }
}
