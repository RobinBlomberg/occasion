/**
 * @file Bundles all files in a directory into one file (lightweight).
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

const fs    = require('fs')
const path  = require('path')
const babel = require('babel-core')

// Check whether a path is a directory:
const isDir = (path) => fs.lstatSync(path).isDirectory()

// Flatten an array recursively:
const flattenDeep = (arr) =>
    arr.reduce((acc, item) =>
        acc.concat(Array.isArray(item) ? flattenDeep(item) : item), [])

// Get all directory file paths recursively:
const readDirDeep = (dir) =>
{
    dir = path.parse(dir).ext !== '' ? path.parse(dir).dir : dir
    return flattenDeep(fs.readdirSync(dir).map(name =>
    {
        const filePath = path.join(dir, name)
        return isDir(filePath) ? readDirDeep(filePath) : filePath
    }))
}

module.exports = function(inPath, outPath, browserQuery)
{
    const file = fs.readFileSync(inPath, 'UTF-8')
    const sep = /\\|\//
    const rootDepth = inPath.split(sep).length - 1
    const relPath = filePath =>
        [ '.', ...filePath.split(sep).slice(rootDepth) ].join('\\\\')

    // Create the output file:
    let output = `
        const resolve = (...paths) =>
        {
            const sep = /\\\\|\\//;
            let oldSplit = [];

            for (let path of paths)
            {
                const newSplit = path.split(sep);
                path = [ ...oldSplit, ...newSplit ];

                if (/^[A-Za-z]:$/.test(newSplit[0]))
                {
                    path.splice(0, oldSplit.length);
                    oldSplit = [];
                }

                for (let i = 0; i < path.length; i++)
                {
                    if (path[i] === '..')
                    {
                        path.splice(i - 1, 2);
                        i -= 1;
                    }
                    else if (path[i] === '.')
                    {
                        path.splice(i, 1);
                        i -= 1;
                    }
                }
                oldSplit = path;
            }

            return [ '.', ...oldSplit ].join('\\\\');
        };

        const module = {
            stack: [resolve('${relPath(inPath)}', '..')],
            exports: null,
            imports: {
            ${readDirDeep(inPath).map(fileName =>
                `    '${relPath(fileName)}'()
                {
                    ${fs.readFileSync(fileName, 'UTF-8').trim()}
                }`
            ).join(',\r\n    ')}
            }
        };

        const require = ref =>
        {
            const last = module.stack.length > 0
                ? module.stack[module.stack.length - 1]
                : '';
            let id = resolve(last, ref);

            if (!module.imports.hasOwnProperty(id) && !/\\.js$/.test(id))
            {
                id = id + '.js';
            }

            if (!module.imports.hasOwnProperty(id))
            {
                throw new Error('Cannot find module \\'' + ref + '\\'');
            }

            module.stack.push(resolve(id, '..'));
            module.imports[id]();
            module.stack.pop();
            return module.exports;
        };

        module.imports['${relPath(inPath)}']();`

    // Transpile to older browsers:
    if (browserQuery !== undefined)
    {
        output = babel.transform(output, {
            presets: [
                [require('babel-preset-env'), {
                    targets: {
                        browsers: browserQuery.split(/\s*,\s*/)
                    },
                    useBuiltIns: true
                }]
            ]
        }).code
    }

    // Write the bundled file:
    if (outPath)
    {
        fs.writeFileSync(outPath, output)
    }

    return output
}
