/**
 * @file Automatically hyphenates long words.
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

module.exports = class Hyphenator
{
    constructor()
    {
        // Feature detection:
        if (this.supportsHyphens)
        {
            return true
        }

        // Create hyphenator:
        const softHyphen = '\u00ad'
        const pattern = hyphenationPatternsSv
        const hyphenate = createHyphenator(pattern, { hyphenChar: softHyphen })
        const minLen = 10

        // Hyphenate all long words in headings:
        $('.heading').each((i, $heading) =>
        {
            $($heading.childNodes).each((i, node) =>
            {
                if (node.nodeType !== Node.TEXT_NODE)
                {
                    return false
                }

                // Hyphenate long words:
                node.nodeValue = node.nodeValue
                    .trim()
                    .split(/\s+/)
                    .map(word => word.length >= minLen ? hyphenate(word) : word)
                    .join(' ')
            })
        })
    }

    // Hyphen support detection.
    // Adapted from Hyphenator:
    // https://github.com/mnater/Hyphenator/blob/master/Hyphenator_Loader.js
    get supportsHyphens()
    {
        const lang = 'sv'
        const fakeBody = document.createElement('body')
        const shadowContainer = document.createElement('div')
        const shadow = document.createElement('div')
        const textNode = document.createTextNode('nationalencyklopedin')
        let height = 0

        shadowContainer.style.visibility = 'hidden'
        fakeBody.appendChild(shadowContainer)
        document.documentElement.appendChild(fakeBody)
        shadow.style.MozHyphens = 'auto'
        shadow.style['-webkit-hyphens'] = 'auto'
        shadow.style['-ms-hyphens'] = 'auto'
        shadow.style.hyphens = 'auto'
        shadow.style.width = '5em'
        shadow.style.lineHeight = '12px'
        shadow.style.border = 'none'
        shadow.style.padding = '0'
        shadow.style.wordWrap = 'normal'
        shadow.style.wordBreak = 'normal' // Fix not present in Hyphenator.
        shadow.style['-webkit-locale'] = `'${lang}'`
        shadow.lang = lang
        shadow.appendChild(textNode)
        shadowContainer.appendChild(shadow)
        height = shadow.offsetHeight
        fakeBody.parentNode.removeChild(fakeBody)

        return height > 13
    }
}
