/**
 * @file Handles the modal logic.
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

const _ = require('./util')

module.exports = class Modal
{
    static handleOutsideClicks(modals, modalLikes)
    {
        // Handle escape:
        $(window).on('keydown', e =>
        {
            if (e.key === 'Escape')
            {
                Object.values(modals).map(modal => modal.close())
                Object.values(modalLikes).map(modalLike => modalLike.close())
            }
        })

        // Handle outside clicks:
        $(window).on('click', e =>
        {
            const path = _.getComposedPath(e.originalEvent)

            // Modals:
            Object.values(modals).map(modal =>
            {
                const hasButton = $button => path.includes($button)
                const isButtonChild = e.target.dataset.insideButton != null
                const isButtonClick = isButtonChild // IE11 support.
                    || Array.from(modal.$button).some(hasButton)
                const isOutsideClick = !path.includes(modal.$content[0])
                if (modal.isOpen && !isButtonClick && isOutsideClick)
                {
                    modal.close()
                }
            })

            // Modal-likes:
            Object.values(modalLikes).map(modalLike =>
            {
                const isElementClick = path.includes(modalLike.$el[0])
                if (modalLike.isOpen && !isElementClick)
                {
                    modalLike.close()
                }
            })
        })
    }

    constructor(el, button)
    {
        this.$el            = $(el)
        this.$button        = $(button)
        this.$ctaButton     = $('.menu-item.cta')[0]
        this.$contactButton = $('.menu-button-contact')[0]
        this.$body          = $('body')
        this.$bodyContent   = $('.content')
        this.$menu          = $('.menu')
        this.$overlay       = $('.modal-overlay')
        this.$content       = this.$el.find('.modal-content')
        this.$arrow         = this.$el.find('.modal-arrow')
        this.$buttonClose   = this.$el.find('.modal-button-close')

        this.ctaButtonContent = this.$ctaButton.innerHTML
        this.contactButtonContent = this.$contactButton.innerHTML

        this.$button.on('click', e =>
        {
            e.preventDefault()
            this.toggle(e)
        })

        this.$buttonClose.on('click', e => this.close())

        $(window).on('resize', e => this.positionArrow())
    }

    get isOpen()
    {
        return this.$el.hasClass('expanded')
    }

    toggle(e)
    {
        if (this.isOpen)
        {
            return this.close(e)
        }
        else
        {
            return this.open(e)
        }
    }

    open(e)
    {
        const c_p = this.modalPadding
        const sb_w = _.scroll.scrollbarWidth
        this.$body.addClass('modal-open')
        this.$bodyContent.css('padding-right', `${sb_w}px`)
        this.$menu.css('padding-right', `${sb_w}px`)
        this.$overlay.addClass('dimmed')
        this.$el.addClass('expanded')
        this.$content.focus()
        this.positionArrow()
        this.$el.css('pointer-events', 'auto')

        const path = _.getComposedPath(e)
        if (path.includes(this.$ctaButton))
        {
            this.$ctaButton.innerHTML = '<i class="mdi mdi-close" style="font-size: 24px; vertical-align: -5px"></i>'
        }
        else if (path.includes(this.$contactButton))
        {
            this.$contactButton.innerHTML = '<i class="mdi mdi-close" style="font-size: 24px; vertical-align: -3px"></i>'
        }

        return true
    }

    close(e)
    {
        this.$body.removeClass('modal-open')
        this.$bodyContent.css('padding-right', 0)
        this.$menu.css('padding-right', 0)
        this.$overlay.removeClass('dimmed')
        this.$el.removeClass('expanded')
        this.$ctaButton.innerHTML = this.ctaButtonContent
        this.$contactButton.innerHTML = this.contactButtonContent
        this.$el.css('pointer-events', 'none')
        return false
    }

    positionArrow()
    {
        if (this.$arrow.length > 0 && this.$button.length > 0)
        {
            this.$arrow.css({ left: this.$ctaButton.offsetLeft + 30 + 'px' })
        }
    }
}
