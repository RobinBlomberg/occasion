/**
 * @file Enables smooth scrolling on link clicks.
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

module.exports = class Scroll
{
    constructor(menu)
    {
        // Auto-scroll on page load:
        window.addEventListener('load', () =>
        {
            if (location.hash.length > 0)
            {
                Scroll.to(location.hash, menu)
            }
        })
    }

    static to(hash, menu, e)
    {
        // Undefined section:
        if ($(hash).length === 0)
        {
            return false
        }

        // If it's an event, don't jump to the link URL:
        if (e !== undefined)
        {
            e.preventDefault()
        }

        // Update the browser history:
        if (history.pushState)
        {
            history.pushState(null, null, hash)
        }
        else
        {
            location.hash = hash
        }

        // Scroll to the specified section:
        $('html, body').animate({
            // Takes padding, menu height and pixel visibility into account:
            scrollTop: $(hash).offset().top - 56 - menu.height + 1
        }, 1200)
    }
}
