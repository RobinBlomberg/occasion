/**
 * @file The entry file.
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

const Menu       = require('./menu')
const Animator   = require('./animator')
const Carousel   = require('./carousel')
const Modal      = require('./modal')
const Services   = require('./services')
const Arrow      = require('./arrow')
const Scroll     = require('./scroll')
const Hyphenator = require('./hyphenator')
const Preloader  = require('./preloader')
const Lazyloader = require('./lazyloader')

// OFI:
objectFitImages()

const modals = {
    contact: new Modal('.contact', '[data-action="open-contact"]'),
    booking: new Modal('.booking', '[data-action="open-booking"]'),
    downloadPdf: new Modal('.download-pdf', 'a[href="/#download-pdf"]'),
}
const modalLikes = {
    menu: new Menu()
}
Modal.handleOutsideClicks(modals, modalLikes)

new Animator()
new Carousel()
new Services(modalLikes.menu)
new Arrow()
new Scroll(modalLikes.menu)
new Hyphenator()
new Preloader()
new Lazyloader()
