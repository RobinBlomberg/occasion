/**
 * @file Starts animations on visible animation elements.
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

module.exports = class Animator
{
    constructor()
    {
        this.$elements = $('[data-animate]')
        this.offsetY   = 64

        this.$elements.addClass('animatable')
        this.animate()
        $(window).on('scroll', this.animate.bind(this))
        $(window).on('resize', this.animate.bind(this))
    }

    animate()
    {
        this.$elements.each((i, elem) =>
        {
            const rect = elem.getBoundingClientRect()
            if (rect.top < window.innerHeight - this.offsetY)
            {
                $(elem).addClass('animated')
            }
        })
    }
}
