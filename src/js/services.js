/**
 * @file Handles the links in the "Services" section.
 * @author Robin Blomberg <hello@robinblomberg.se>
 * @copyright Robin Blomberg 2018-
 */

const scroll = require('./scroll')

module.exports = class Services
{
    constructor(menu)
    {
        this.$el = $('.services')
        this.$links = this.$el.find('a')
        this.$links.on('click', e =>
        {
            const hash = e.currentTarget.hash
            if (hash.length > 0)
            {
                scroll.to(hash, menu, e)
            }
        })
    }
}
